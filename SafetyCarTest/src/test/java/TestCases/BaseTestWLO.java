package TestCases;

import com.safetycar.framework.Utils;
import com.safetycar.framework.UserAction;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class BaseTestWLO {
    UserAction actions = new UserAction();

    @BeforeClass
    public static void setUp(){
        Utils.LOG.info("Run loadBrowser method Before Class");
        UserAction.loadBrowser();
    }

    @AfterClass
    public static void tearDown(){
        UserAction.quitDriver();
    }
}
