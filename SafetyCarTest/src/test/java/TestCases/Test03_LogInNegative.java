package TestCases;

import com.safetycar.framework.Utils;
import com.sun.org.glassfish.gmbal.Description;
import org.junit.Test;

public class Test03_LogInNegative extends BaseTestWLO {
    private String userName = Utils.getUIMappingByKey("LogInPage.UserName");
    private String locatorUserName = Utils.getUIMappingByKey("LogInPage.LocatorUserName");
    private String password= Utils.getUIMappingByKey("LogInPagePassword");
    private String locatorPassword = Utils.getUIMappingByKey("LogInPage.LocatorPassword");
    private String locatorLogInButton = Utils.getUIMappingByKey("LoginPage.LocatorLoginButton");
    private String locatorPasswordAlert=Utils.getUIMappingByKey("LogInPage.PasswordAlert");
    private String locatorWrongPassword=Utils.getUIMappingByKey("LogInPage.BadCredentials");
    private String LocatorWrongUsername=Utils.getUIMappingByKey("LogInPage.WrongUserName");
    private String LocatorBlankUserName=Utils.getUIMappingByKey("LogInPage.BlankUserName");

    @Test
    @Description("Validate that user is unable to log in with blank password. ")
    public void LoginWithBlankPassword_ClickRegisterButton_VisibleErrorMessage() {

        actions.goToLogInPage();
        actions.LogInWithMissingCredentials(userName, locatorUserName, "",//Blank password
                locatorPassword,locatorLogInButton);

        actions.assertElementPresent(locatorPasswordAlert);
    }

    @Test
    @Description("Validate that user is unable to log in with wrong password. ")
    public void LoginWithWrongPassword_ClickRegisterButton_VisibleErrorMessage() {

        actions.goToLogInPage();
        actions.LogInWithMissingCredentials(userName, locatorUserName, "1234567",//Wrong password
                locatorPassword,locatorLogInButton);

        actions.assertElementPresent(locatorWrongPassword);
    }

    @Test
    @Description("Validate that user is unable to log in with wrong username. ")
    public void LoginWithWrongUsername_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToLogInPage();
        actions.LogInWithMissingCredentials("henry", //Wrong user name
                locatorUserName, password,locatorPassword,locatorLogInButton);

        actions.assertElementPresent(LocatorWrongUsername);
    }

    @Test
    @Description("Validate that user is unable to log in with blank username. ")
    public void LoginWithBlankUsername_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToLogInPage();
        actions.LogInWithMissingCredentials("", //Blank user name
                locatorUserName, password,locatorPassword,locatorLogInButton);

        actions.assertElementPresent(LocatorBlankUserName);
    }

    @Test
    @Description("Validate that user is unable to log in with blank username and blank password. ")
    public void LoginWithBlankUsernameAndBlankPassword_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToLogInPage();
        actions.LogInWithMissingCredentials("", //Blank user name
                locatorUserName, "",            //Blank password
                locatorPassword,locatorLogInButton);

        actions.assertElementPresent(LocatorBlankUserName);
        actions.assertElementPresent(locatorPasswordAlert);
    }

    @Test
    @Description("Validate that user is unable to log in with wrong username and wrong password. ")
    public void LoginWithWrongUsernameAndWrongPassword_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToLogInPage();
        actions.LogInWithMissingCredentials("wrong", //Wrong user name
                locatorUserName, "1234567",           //Wrong password
                locatorPassword,locatorLogInButton);

        actions.assertElementPresent(LocatorWrongUsername);
    }

}
