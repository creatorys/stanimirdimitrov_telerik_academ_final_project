package TestCases;

import com.safetycar.framework.Utils;
import com.sun.org.glassfish.gmbal.Description;
import org.junit.Test;

public class Test04_LoginPositive extends BaseTestWLO {

    private String userName = Utils.getUIMappingByKey("LogInPage.UserName");
    private String locatorUserName = Utils.getUIMappingByKey("LogInPage.LocatorUserName");
    private String password= Utils.getUIMappingByKey("LogInPagePassword");
    private String locatorPassword = Utils.getUIMappingByKey("LogInPage.LocatorPassword");
    private String locatorLogInButton = Utils.getUIMappingByKey("LoginPage.LocatorLoginButton");
    private String LocatorHomePageUseName=Utils.getUIMappingByKey("HomePage.ProfileName");


    @Test
    @Description("Validate that user is able to log in with valid username and password. ")
    public void LoginWithValidUserNameAndPassword_ClickRegisterButton_RedirectToHomePage() {

        actions.goToLogInPage();
        actions.LogInWithCorrectCredentials(userName, locatorUserName, password, locatorPassword,locatorLogInButton);

        actions.waitForElementVisible(LocatorHomePageUseName,2);
        actions.assertElementPresent(LocatorHomePageUseName);
    }

}
