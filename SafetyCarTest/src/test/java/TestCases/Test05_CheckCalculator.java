package TestCases;

import com.safetycar.framework.ReadExcelFile;
import com.safetycar.framework.Utils;
import com.sun.org.glassfish.gmbal.Description;
import org.apache.poi.ss.usermodel.DateUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import sun.swing.UIAction;
import java.text.SimpleDateFormat;
import java.util.Date;

@RunWith(Parameterized.class)
@Category(UIAction.class)
public class Test05_CheckCalculator extends BaseTestWLO {
    WebDriver driver = Utils.getWebDriver();

    //Data Source
    @Parameterized.Parameters
        public static Object[][] testDataFeed(){
        Utils.LOG.info("Start test data feed method");

        ReadExcelFile config= new ReadExcelFile();
        //config.readExcelFile("C://excel.xlsx");
       // String path = System.getProperty("user.dir") + "\\src\\test\\resources\\externalFiles\\excel.xlsx";
        config.readExcelFile(System.getProperty("user.dir") + "\\src\\test\\resources\\externalFiles\\excel.xlsx");

        int rows = config.getRowCount(0);

        Object[][] data = new Object[rows][7];
        for (int i =0; i<rows;i++){
            data[i][0] = config.getData(0,i,0);
            data[i][1] = config.getData(0,i,1);
            data[i][2] = config.getData(0,i,2);
            data[i][3] = config.getData(0,i,3);
            data[i][4] = config.getData(0,i,4);
            data[i][5] = config.getData(0,i,5);
            data[i][6] = config.getData(0,i,6);
        }
        return data;
    }

    @Test
    @ParameterizedTest
    @MethodSource("testDataFeed")
    @Description("Check calculations on Insurance calculator ")
    public void InputDataInCalculator_ClickCalculate_DisplayTotalPremium(String carMake, String carModel,
                                                                         String cubicCapacity, String firstRegistration,
                                                                         String hasAccident, String driverAge,
                                                                         String totalPremium) throws InterruptedException
    {

        actions.goToLandingPage();
        actions.clickElement(Utils.getUIMappingByKey("LogInPage.CalculatorLink"));
        Thread.sleep(500);
        actions.clickElement(Utils.getUIMappingByKey("LoginPage.InsuranceCalculator"));
        Thread.sleep(500);
        actions.assertElementPresent(Utils.getUIMappingByKey("CalculatorPage.Test"));

        //Car make
        Select selectCarMake = new Select(driver.findElement(By.xpath("//select[@id='carBrand']")));
        selectCarMake.selectByVisibleText(carMake);
        Utils.LOG.info(carMake);
        //Car model
        Select selectCarModel = new Select(driver.findElement(By.xpath("//select[@id='carModel']")));
        selectCarModel.selectByVisibleText(carModel);
        Utils.LOG.info(carModel);
        //Cubic capacity
        actions.typeValueInField(cubicCapacity,Utils.getUIMappingByKey("CalculatorPage.CubicCapacity"));
        //First registration
        actions.typeValueInField(convertStringToDate(firstRegistration),Utils.getUIMappingByKey("CalculatorPage.FirstRegistration"));
        //Had accident
        if(hasAccident.equals("CHECK")){ actions.clickElement(Utils.getUIMappingByKey("CalculatorPae.CheckBox"));
        }
        //Driver Age
        actions.clearTextInField("CalculatorPage.DriverAge");
        actions.typeValueInField(driverAge,Utils.getUIMappingByKey("CalculatorPage.DriverAge"));
        //Click on button calculate
        actions.clickElement(Utils.getUIMappingByKey("CalculatorPae.CalculateButton"));
        Thread.sleep(500);
        Utils.LOG.info("Result: " + actions.getTextFromFindElement(Utils.getUIMappingByKey("CalculatorPage.TotalPremium")) + " expected result: " + totalPremium);
        Assert.assertEquals(actions.getTextFromFindElement(Utils.getUIMappingByKey("CalculatorPage.TotalPremium")),totalPremium);


       // actions.checkingInsuranceCalculator(carMake,carModel, cubicCapacity, firstRegistration,hasAccident, driverAge, totalPremium);
    }
    private String convertStringToDate(String firstRegistration){
        Double d = Double.parseDouble(firstRegistration);
        Date javaDate = DateUtil.getJavaDate((double)d);
        String convertedDate = new SimpleDateFormat("yyyy-MM-dd").format(javaDate);
        return convertedDate;

    }



}
