package TestCases;

import com.safetycar.framework.Utils;
import com.sun.org.glassfish.gmbal.Description;
import org.junit.Test;

public class Test02_CreateAccountPositive  extends BaseTestWLO {
    private String firstName = Utils.getUIMappingByKey("registration_UserOne.FirstName");
    private String lfn = Utils.getUIMappingByKey("registration.FirstName");
    private String lastName = Utils.getUIMappingByKey("registration_UserOne.LastName");
    private String lln = Utils.getUIMappingByKey("registration.LastName");
    private String email = Utils.getUIMappingByKey("registration_UserOne.Email");
    private String lem = Utils.getUIMappingByKey("registration.Email");
    private String phone = Utils.getUIMappingByKey("registration_UserOne.PhoneNumber");
    private String lPhone = Utils.getUIMappingByKey("registration.PhoneNumber");
    private String address = Utils.getUIMappingByKey("registration_UserOne.Address");
    private String lad = Utils.getUIMappingByKey("registration.Address");
    private String userName = Utils.getUIMappingByKey("registration_UserOne.UserName");
    private String lun = Utils.getUIMappingByKey("registration.UserName");
    private String password1 = Utils.getUIMappingByKey("registration_UserOne.PasswordOne");
    private String lp1 = Utils.getUIMappingByKey("registration.PasswordOne");
    private String password2 = Utils.getUIMappingByKey("registration_UserOne.PasswordTwo");
    private String lp2 = Utils.getUIMappingByKey("registration.PasswordTwo");
    private String asserting = Utils.getUIMappingByKey("registration.fieldIsEmpty");


    @Test
    @Description("Validate that user is able to register with valid credentials .")
    public void WithValidCredentials_ClickRegisterButton_UserMustBeRedirectToHomePage() {
        actions.goToTheRegisterPage();
        actions.registerWithValidCredentials(
                "Harry", lfn,
                "Williams", lln,
                "hariewilliams@dir.bg", lem,
                phone, lPhone,
                address, lad,
                "HarryWilliams", lun,
                password1, lp1,
                password2, lp2,
                asserting);
        actions.waitForElementVisible("LogInPage.LogInButton",2);
        actions.assertElementPresent("LogInPage.LogInButton");
    }

    @Test
    @Description("Validate that user is able to register with post address from five characters  .")
    public void WithAddressFromFiveCharacters_ClickRegisterButton_UserMustBeRedirectToLogInPage() {
        actions.goToTheRegisterPage();
        actions.registerWithValidCredentials(
                "Harrie", lfn,
                "Williamson", lln,
                "harriewilliamson@dir.bg", lem,
                phone, lPhone,
                "Sofia", lad,
                "HarrieWilliamson", lun,
                password1, lp1,
                password2, lp2,
                asserting);
        //actions.waitForElementVisible("",2);
        //actions.assertElementPresent("");
    }

    @Test
    @Description("Validate that user is able to register with post address from 50 characters  .")
    public void WithAddressFromFiftyCharacters_TheSystemAcceptsTheEnteredData() {
        actions.goToTheRegisterPage();
        actions.registerWithValidCredentials(
                "Hans", lfn,
                "Fifties", lln,
                "hansfifties@dir.bg", lem,
                phone, lPhone,
                "Bulgaria, Sofia, Aleksander Malinov Blvd.number 50", lad, //Address is 50 characters long
                "HasFifties", lun,
                password1, lp1,
                password2, lp2,
                asserting);
        //actions.waitForElementVisible("",2);
        //actions.assertElementPresent("");
    }

    @Test
    @Description("Validate that user is able to register with username length from 6 characters  .")
    public void WithUserNameFromSixCharacters_ClickRegisterButton_TheSystemAcceptsTheEnteredData() {
        actions.goToTheRegisterPage();
        actions.registerWithValidCredentials(
                "Henry", lfn,
                "Six", lln,
                "henrysix@dir.bg", lem,
                phone, lPhone,
                address, lad,
                "Henry6", lun,
                password1, lp1,
                password2, lp2,
                asserting);
        //actions.waitForElementVisible("",2);
        //actions.assertElementPresent("");
    }

    @Test
    @Description("Validate that user is able to register with username length from 30 characters.")
    public void WithUserNameFromThirtyCharacters_ClickRegisterButton_TheSystemAcceptsTheEnteredData() {
        actions.goToTheRegisterPage();
        actions.registerWithValidCredentials(
                "Henry", lfn,
                "Thirty", lln,
                "henryThirty@dir.bg", lem,
                phone, lPhone,
                address, lad,
                "HenryThirtyMustBeAtLeastThirty", lun,
                password1, lp1,
                password2, lp2,
                asserting);
        //actions.waitForElementVisible("",2);
        //actions.assertElementPresent("");
    }
}

