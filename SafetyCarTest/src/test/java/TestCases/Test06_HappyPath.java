package TestCases;

import com.google.common.base.Verify;
import com.safetycar.framework.Utils;
import com.sun.org.glassfish.gmbal.Description;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import sun.invoke.util.VerifyType;

public class Test06_HappyPath extends BaseTestWLO {
private String pathToImage = System.getProperty("user.dir") + "\\src\\test\\resources\\externalFiles\\carCertificate.jpg";
    @Test
    @Description("Unregister user send insurance request,  make registration and check policy")
    public void UnregisterUser_makeInsurancePolicy_ShowInsurancePolicy() throws InterruptedException {
        actions.goToLandingPage();
        actions.clickElement(Utils.getUIMappingByKey("LogInPage.CalculatorLink"));
        Thread.sleep(500);
        actions.clickElement(Utils.getUIMappingByKey("LoginPage.InsuranceCalculator"));
        //Car make
        Select selectCarMake = new Select(actions.findElement("CalculatorPage.CarBrand"));
        selectCarMake.selectByVisibleText("FIAT");
        Utils.LOG.info("FIAT");
        //Car model
        Select selectCarModel = new Select(actions.findElement("CalculatorPage.CarModel"));
        selectCarModel.selectByVisibleText("500");
        Utils.LOG.info(500);
        //Cubic capacity
        actions.typeValueInField("1600",Utils.getUIMappingByKey("CalculatorPage.CubicCapacity"));
        //First registration
        actions.typeValueInField("2012-10-25",Utils.getUIMappingByKey("CalculatorPage.FirstRegistration"));
        //Had accident
        actions.clickElement(Utils.getUIMappingByKey("CalculatorPae.CheckBox"));
        //Driver Age
        actions.clearTextInField(Utils.getUIMappingByKey("CalculatorPage.DriverAge"));
        actions.typeValueInField("26",Utils.getUIMappingByKey("CalculatorPage.DriverAge"));
        //Click on button calculate
        actions.clickElement(Utils.getUIMappingByKey("CalculatorPae.CalculateButton"));
        Thread.sleep(500);
        Utils.LOG.info("Result: " + actions.getTextFromFindElement(Utils.getUIMappingByKey("CalculatorPage.TotalPremium")) + " expected result: 912.08 BGN");
        Assert.assertEquals(actions.getTextFromFindElement(Utils.getUIMappingByKey("CalculatorPage.TotalPremium")),"912.08 BGN");
        actions.clickElement("registration.GenerateOffer"); // Click on Generate offer button
        //-------------Switch to active tab ------------------\\
                    actions.switchBetweenBrowserTab();
        //------------------------------------------------------\\
        //-------------------Login Page ------------------ Login form -----------
        //Type username in login form
        actions.waitForElementVisible("LogInPage.LocatorUserName",2);
        actions.typeValueInField("Henry6","LogInPage.LocatorUserName");
        //Type password in login form
        actions.typeValueInField("123456789","LogInPage.LocatorPassword");
        //Click login button
        actions.clickElement("LogInPage.LogInButton");
        actions.assertElementPresent("LogInPage.LogInButton");
    }
}
