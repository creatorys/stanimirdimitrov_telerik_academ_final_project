package TestCases;
import com.safetycar.framework.Utils;
import com.sun.org.glassfish.gmbal.Description;
import org.junit.Test;
import org.openqa.selenium.By;

public class Test01_CreateAccountNegative extends BaseTestWLO {
    private String firstName = Utils.getUIMappingByKey("registration_UserOne.FirstName");
    private String lfn = Utils.getUIMappingByKey("registration.FirstName");
    private String lastName = Utils.getUIMappingByKey("registration_UserOne.LastName");
    private String lln = Utils.getUIMappingByKey("registration.LastName");
    private String email = Utils.getUIMappingByKey("registration_UserOne.Email");
    private String lem = Utils.getUIMappingByKey("registration.Email");
    private String phone = Utils.getUIMappingByKey("registration_UserOne.PhoneNumber");
    private String lPhone = Utils.getUIMappingByKey("registration.PhoneNumber");
    private String address = Utils.getUIMappingByKey("registration_UserOne.Address");
    private String lad = Utils.getUIMappingByKey("registration.Address");
    private String userName = Utils.getUIMappingByKey("registration_UserOne.UserName");
    private String lun = Utils.getUIMappingByKey("registration.UserName");
    private String password1 = Utils.getUIMappingByKey("registration_UserOne.PasswordOne");
    private String lp1 = Utils.getUIMappingByKey("registration.PasswordOne");
    private String password2 = Utils.getUIMappingByKey("registration_UserOne.PasswordTwo");
    private String lp2 = Utils.getUIMappingByKey("registration.PasswordTwo");
    private String asserting = Utils.getUIMappingByKey("registration.fieldIsEmpty");

    @Test
    @Description("Validate that user is unable to register with valid data and blank mail. ")
    public void OnBlankEmail_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName, lfn, lastName, lln,
                "",//Blank email
                lem, phone, lPhone, address, lad, userName, lun, password1, lp1, password2, lp2, asserting);
    }

    @Test
    @Description("Validate that user is unable to register with valid data and blank first name.")
    public void OnBlankFirstName_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields("", //Blank first name
                lfn, lastName, lln, email,
                lem, phone, lPhone, address, lad, userName, lun, password1, lp1, password2, lp2,asserting);

    }

    @Test
    @Description("Validate that user is unable to register with valid data and blank last name.")
    public void OnBlankLastName_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName, lfn, "",//blank last name
                lln, email,
                lem, phone, lPhone, address, lad, userName, lun, password1, lp1, password2, lp2,asserting);
    }

    @Test
    @Description("Validate that user is unable to register with valid data and blank username.")
    public void OnBlankUserName_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName, lfn, lastName, lln, email,
                lem, phone, lPhone, address, lad, "", //blank user name
                lun, password1, lp1, password2, lp2,asserting);
    }
    @Test
    @Description("Validate that user is unable to register with valid data and blank password.")
    public void OnBlankPassword_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName, lfn, lastName, lln, email,
                lem, phone, lPhone, address, lad, userName,
                lun, "", lp1, "", //blank password
                lp2,asserting);
    }

    @Test
    @Description("Validate that user is unable to register with valid data and different password in confirmation field.")
    public void DifferentPassword_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName, lfn, lastName, lln, email,
                lem, phone, lPhone, address, lad, userName,
                lun,password1, lp1, "1234567891", //different password password
                lp2,asserting);
        actions.assertElementPresent("registration.passwordNotMatch");
    }

    @Test
    @Description("Validate that user is unable to register with valid data and short first name.")
    public void ShortFirstName_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields("J", //Short first name
                lfn, lastName, lln, email,
                lem, phone, lPhone, address, lad, userName, lun, password1, lp1, password2, lp2,asserting);
        actions.waitForElementVisible("registration.nameLength",1);
        actions.assertElementPresent("registration.nameLength");
    }

    @Test
    @Description("Validate that user is unable to register with valid data and Long first name.")
    public void LongFirstName_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields("Jackismyfirstnamebutineedmorelongnameatleastfift-51", //Long first name
                lfn, lastName, lln, email,
                lem, phone, lPhone, address, lad, userName, lun, password1, lp1, password2, lp2,asserting);
        actions.waitForElementVisible("registration.nameLength",1);
        actions.assertElementPresent("registration.nameLength");
        }
    @Test
    @Description("Validate that user is unable to register with valid data and Long last name.")
    public void LongLastName_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName,
                lfn, "Jackismyfirstnamebutineedmorelongnameatleastfift-51", //Long first name
                lln, email, lem, phone, lPhone, address, lad, userName, lun, password1, lp1, password2, lp2,asserting);
        actions.waitForElementVisible("registration.nameLength",2);
        actions.assertElementPresent("registration.nameLength");
    }

    @Test
    @Description("Validate that user is unable to register with valid data and short last name.")
    public void ShortLastName_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName,
                lfn, "J", //short first name
                lln, email, lem, phone, lPhone, address, lad, userName, lun, password1, lp1, password2, lp2,asserting);
        actions.waitForElementVisible("registration.nameLength",2);
        actions.assertElementPresent("registration.nameLength");
    }

    @Test
    @Description("Validate that user is unable to register with valid data and wrong mail format. ")
    public void WrongEmailFormat_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName, lfn, lastName, lln,
                "mail#mail.bg",//Wrong email format
                lem, phone, lPhone, address, lad, userName, lun, password1, lp1, password2, lp2, asserting);
        //TODO
    }

    @Test
    @Description("Validate that user is unable to register with valid data and with letters in phone number.")
    public void WrongPhoneNumberFormat_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName,
                lfn, lastName, lln, email, lem, "0888a23456",
                lPhone, address, lad, userName, lun, password1, lp1, password2, lp2,asserting);
        actions.waitForElementVisible("registration.WrongPhoneNumber",2);
        actions.assertElementPresent("registration.WrongPhoneNumber");

    }

    @Test
    @Description("Validate that user is unable to register with valid data and with short phone number.")
    public void ShortPhoneNumber_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName,
                lfn, lastName, lln, email, lem, "088",
                lPhone, address, lad, userName, lun, password1, lp1, password2, lp2,asserting);
        actions.waitForElementVisible("registration.PhoneLength",2);
        actions.assertElementPresent("registration.PhoneLength");
    }

    @Test
    @Description("Validate that user is unable to register with valid data and with Long phone number.")
    public void LongPhoneNumber_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName,
                lfn, lastName, lln, email, lem, "+359 888 123 416",//Long number 16 symbols
                lPhone, address, lad, userName, lun, password1, lp1, password2, lp2,asserting);
        actions.waitForElementVisible("registration.PhoneLength",2);
        actions.assertElementPresent("registration.PhoneLength");
    }

    @Test
    @Description("Validate that user is unable to register with valid data and with short address length.")
    public void ShortAddress_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName,
                lfn, lastName, lln, email, lem, phone, lPhone, "adrs",// short address 4 symbols
                lad, userName, lun, password1, lp1, password2, lp2,asserting);
        actions.waitForElementVisible("registration.AddressLength",2);
        actions.assertElementPresent("registration.AddressLength");
    }

    @Test
    @Description("Validate that user is unable to register with valid data and with Long address length.")
    public void LongAddress_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName,
                lfn, lastName, lln, email, lem, phone, lPhone, "Bulgaria, Sofia, Aleksander Malinov Blvd. number 51",// short address 51 symbols
                lad, userName, lun, password1, lp1, password2, lp2,asserting);
        actions.waitForElementVisible("registration.AddressLength",2);
        actions.assertElementPresent("registration.AddressLength");
    }
    @Test
    @Description("Validate that user is unable to register with valid data and with address with special characters.")
    public void AddressWithSpecialCharacters_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName,
                lfn, lastName, lln, email, lem, phone, lPhone, "@@//,,..'++123--$$(())%%Malinov Blvd. № 51",// short address 51 symbols
                lad, userName, lun, password1, lp1, password2, lp2,asserting);
    }

    @Test
    @Description("Validate that user is unable to register with valid data and with short username length.")
    public void ShortUserName_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName,
                lfn, lastName, lln, email, lem, phone, lPhone, address, lad, "Jacky", //Short user name 5 symbols
                lun, password1, lp1, password2, lp2,asserting);
        actions.waitForElementVisible("registration.UserNameLength",2);
        actions.assertElementPresent("registration.UserNameLength");
    }

    @Test
    @Description("Validate that user is unable to register with valid data and with Long username length.")
    public void LongUserName_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName,
                lfn, lastName, lln, email, lem, phone, lPhone, address, lad, "JackSmithIsUserFromBulgariaBG31", //Long user name 31 symbols
                lun, password1, lp1, password2, lp2,asserting);
        actions.waitForElementVisible("registration.UserNameLength",2);
        actions.assertElementPresent("registration.UserNameLength");
    }

    @Test
    @Description("Validate that user is unable to register with valid data and with username with special characters.")
    public void UserNameWithSpecialCharacters_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName,
                lfn, lastName, lln, email, lem, phone, lPhone, address, lad, "'1+1'='1+1'-+*/()$%#@", //user name with special characters
                lun, password1, lp1, password2, lp2,asserting);
    }

    @Test
    @Description("Validate that user is unable to register with valid data and short password.")
    public void ShortPassword_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName, lfn, lastName, lln, email,
                lem, phone, lPhone, address, lad, userName,
                lun,"123456", lp1, "123456", //short password
                lp2,asserting);
        actions.waitForElementVisible("registration.PasswordLength",2);
        actions.assertElementPresent("registration.PasswordLength");
    }

    @Test
    @Description("Validate that system constraints to create safe password.")
    public void ConstrainsForSafePassword_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName, lfn, lastName, lln, "Jack@dir.bg",
                lem, phone, lPhone, address, lad, userName,
                lun,"1111111", lp1, "1111111", //short password
                lp2,asserting);
        String a = actions.findElement("registration.EmailInUse").getText();
        if(a.equals("Username is already in use")){
            actions.waitForElementVisible("registration.EmailInUse",2);
            actions.assertElementPresent("registration.EmailInUse");
        }else{
            actions.assertElementPresent("It's error");
        }

    }

    @Test
    @Description("Validate that user is unable to register with email that is already registered.")
    public void RegisteredEmail_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName, lfn, lastName, lln, "Jack@dir.bg", //email is already registered in previous
                lem, phone, lPhone, address, lad, userName,
                lun,"1111111", lp1, "1111111",
                lp2,asserting);
        actions.waitForElementVisible("registration.EmailInUse",2);
        actions.assertElementPresent("registration.EmailInUse");
    }
    @Test
    @Description("Validate that user is unable to register with username that is already registered.")
    public void RegisteredUserName_ClickRegisterButton_VisibleErrorMessage() {
        actions.goToTheRegisterPage();
        actions.registerWithBlankOrWrongFields(firstName, lfn, lastName, lln, "Jack1@dir.bg",
                lem, phone, lPhone, address, lad, userName,//username is already registered in previous
                lun,"1111111", lp1, "1111111",
                lp2,asserting);
        actions.waitForElementVisible("registration.UserNameInUse",2);
        actions.assertElementPresent("registration.UserNameInUse");
    }
}