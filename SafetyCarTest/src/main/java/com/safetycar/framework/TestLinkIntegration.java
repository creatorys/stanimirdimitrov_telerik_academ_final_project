package com.safetycar.framework;
import testlink.api.java.client.TestLinkAPIClient;
import testlink.api.java.client.TestLinkAPIException;

public class TestLinkIntegration {
    public static final String TESTLINK_KEY="d9f043dab02b2d99874b199922a27704";
    public static final String TESTLINK_URL = "http://34.67.7.157/index.php";
    public static final String TEST_PROJECT_NAME = "Stanimir Dimitrov - Telerik Academy - QA - Final_project";
    public static final String TEST_PLAN_NAME = "";
    public static final String TEST_CASE_NAME = "";
    public static final String BUILD_NAME = "Version 1.0";

    public static void updateResults (String testCaseName, String exception, String results) throws TestLinkAPIException {
        TestLinkAPIClient testLink = new TestLinkAPIClient(TESTLINK_KEY,TESTLINK_URL);
        testLink.reportTestCaseResult(TEST_PROJECT_NAME,TEST_PLAN_NAME, testCaseName, BUILD_NAME, exception, results);
    }
}
