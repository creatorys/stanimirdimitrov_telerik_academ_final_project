package com.safetycar.framework;

import com.sun.deploy.util.Waiter;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.Set;

public class UserAction {
    final WebDriver driver;

    public UserAction() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser() {
        Utils.LOG.info("Run Method_LoadBrowser_UserAction");
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));

    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    //---------- DRIVER ACTIONS ---------------\\

    public WebElement findElement(String locator) {
        Utils.LOG.info("Finding web element " + locator);
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
        return element;
    }
    public String getTextFromFindElement(String locator){
        String text = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))).getText();
        return text;
    }

    public void clickElement(String key) {
        Utils.LOG.info("Clicking on element " + key);
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(key)));
        element.click();
    }

    public void typeValueInField(String value, String field) {
        Utils.LOG.info("Typing in " + value);
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(field)));
        clearTextInField(field);
        element.sendKeys(value);

    }

    public void clearTextInField(String locator) {
        Utils.LOG.info("Clearing field ");
        findElement(locator).clear();
    }

    public int getSizeOfElements(String locator) {
        Utils.LOG.info("Get size of elements " + locator);
        int numberOfElements = driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size();
        return numberOfElements;
    }

    public String nameOfPage() {

        String pageName = driver.getTitle();

        return pageName;
    }
    public void switchBetweenBrowserTab(){
        Utils.LOG.info("Name of page BEFORE tab switching " + driver.getTitle());
        //This code switch to active tab
        Set<String> allWindowHandles = driver.getWindowHandles();
        for (String windowHandle : allWindowHandles) {
            if (!windowHandle.equals("cannot get the active selected window handle")) {
                driver.switchTo().window(windowHandle);
            }
        }
        Utils.LOG.info("Name of page AFTER tab switching " + driver.getTitle());
    }

    //--------------------- Asserts ----------------------------\\

    public void assertElementPresent(String locator) {
        Utils.LOG.info("Asserting that element " + locator + " is present on the page");
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void assertElementNotPresent(String locator) {
        Utils.LOG.info("Element " + locator + " is NOT present on the page");
        Assert.assertEquals(0, driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).size());
    }

    public void assertElementIsNotVisible(String locator) {
        Utils.LOG.info("Element " + locator + " is not visible");
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
        Assert.assertFalse(element.isDisplayed());
    }

    public void assertElementContainsTest(String text, String locator) {
        Utils.LOG.info("Asserting that element " + locator + " contain text: " + text);
        String textOfPost = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))).getText();
        Assert.assertNotEquals(text, textOfPost);
    }

    public void assertElementDoesNotContainText(String text, String locator) {
        Utils.LOG.info("Asserting that element " + locator + "does NOT contain test: " + text);
        String textOfPost = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))).getText();
        Assert.assertNotEquals(text, textOfPost);
    }

    //------------------------WAITS---------------------------\\
    public void waitForElementClickable(String locator, int seconds) {
        Utils.LOG.info("Waiting for element " + locator + " to be clickable");
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void waitForElementVisible(String locator, int seconds) {
        Utils.LOG.info("Waiting for element " + locator + " to be visible");
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(Utils.getUIMappingByKey(locator))));
    }
    //--------------------USER ACTIONS----------------------\\

    public void goToLandingPage() {
        Utils.LOG.info("Going to landing page");
        driver.get(Utils.getConfigPropertyByKey("base.url"));
    }

    public void goToLogInPage() {
        goToLandingPage();
        Utils.LOG.info("Going to login page");
        waitForElementClickable("landing.logIn", 1000);
        clickElement("landing.logIn");
    }

    public void goToTheRegisterPage() {
        goToLogInPage();
        Utils.LOG.info("Going to register page");
        waitForElementClickable("logInPage.SignUpLink", 1000);
        clickElement("logInPage.SignUpLink");

    }

    // Upload an image in a post
    public void uploadPostImage(String locator, String pathToImage) {
        Utils.LOG.info("Uploading image");
        WebElement uploadElement = findElement(Utils.getUIMappingByKey(locator));
        uploadElement.sendKeys(pathToImage);
    }

    public void logOut() {
        Utils.LOG.info("Logged out");
        waitForElementClickable("homePage.logOutButton", 1000);
        clickElement(("homePage.loOutButton"));
    }
    //Registration. Here are all NEGATIVE registration tests
    public void registerWithBlankOrWrongFields(String firstName, String lfn, String lastName, String lln,
                                                String email, String lem, String phone, String lPhone,
                                                String address, String lad, String userName, String lun,
                                                String password1, String lp1, String password2, String lp2, String asserting){
        //------------------------------
        Utils.LOG.info("Loading Registration page ");
        waitForElementVisible(lfn,2);
        // Registration first name
        clearTextInField(lfn);
        typeValueInField(firstName,lfn);
        // Last name
        clearTextInField(lln);
        typeValueInField(lastName,lln);
        //email
        clearTextInField(lem);
        typeValueInField(email,lem);
        // Phone
        clearTextInField(lPhone);
        typeValueInField(phone,lPhone);
        // Address
        clearTextInField(lad);
        typeValueInField(address,lad);
        // User name
        clearTextInField(lun);
        typeValueInField(userName,lun);
        // Password
       // clearTextInField(lp1);
        Utils.LOG.info(password1);
        typeValueInField(password1,lp1);
        // confirm password
        //clearTextInField(lp2);
        waitForElementVisible(lp2,1000);
        Utils.LOG.info(password2);
        typeValueInField(password2,lp2);
        //Click on blank page to clean password not match error
        if(password2.equals("1234567891")){
            Utils.LOG.info("Confirm Password is different if statement check ");
        }else{
            clickElement(Utils.getUIMappingByKey("registration.container"));
        }

        clickElement("registration.SignUpButton");
        Utils.LOG.info("Validate that the page is the same");
        assertElementPresent("registration.SignUpButton");
        Utils.LOG.info("Validate that field is empty");
        assertElementPresent(asserting);

    }

    //Registration. Here are all POSITIVE registration tests
    public void registerWithValidCredentials(String firstName, String lfn, String lastName, String lln,
                                               String email, String lem, String phone, String lPhone,
                                               String address, String lad, String userName, String lun,
                                               String password1, String lp1, String password2, String lp2, String asserting){
        //------------------------------
        Utils.LOG.info("Loading Registration page! ");
        waitForElementVisible(lfn,1);
        // Registration first name
        clearTextInField(lfn);
        typeValueInField(firstName,lfn);
        // Last name
        clearTextInField(lln);
        typeValueInField(lastName,lln);
        //email
        clearTextInField(lem);
        typeValueInField(email,lem);
        // Phone
        clearTextInField(lPhone);
        typeValueInField(phone,lPhone);
        // Address
        clearTextInField(lad);
        typeValueInField(address,lad);
        // User name
        clearTextInField(lun);
        typeValueInField(userName,lun);
        // Password
        // clearTextInField(lp1);
        Utils.LOG.info(password1);
        typeValueInField(password1,lp1);
        // confirm password
        //clearTextInField(lp2);
        waitForElementVisible(lp2,1000);
        Utils.LOG.info(password2);
        typeValueInField(password2,lp2);
        //Click on blank page to clean password not match error
        if(password2.equals("1234567891")){
            Utils.LOG.info("Confirm Password is different if statement check ");
        }else{
            clickElement(Utils.getUIMappingByKey("registration.container"));
        }

        clickElement("registration.SignUpButton");
        Utils.LOG.info("Validate that the page is the same");
        assertElementNotPresent("registration.SignUpButton");
        Utils.LOG.info("Validate that field is empty");
        assertElementNotPresent(asserting);

    }
    //Login. NEGATIVE
    public void LogInWithMissingCredentials(String userName, String locatorUserName,
                                            String password, String locatorPassword,
                                            String locatorLogInButton){
        Utils.LOG.info("LOADING LOGIN PAGE ");
        waitForElementVisible(locatorUserName,1);
        clearTextInField(locatorUserName);
        typeValueInField(userName,locatorUserName);
        clearTextInField(locatorPassword);
        typeValueInField(password,locatorPassword);
        clickElement(locatorLogInButton);
        assertElementPresent(locatorLogInButton);
    }

    //Login. POSITIVE
    public void LogInWithCorrectCredentials(String userName, String locatorUserName,
                                            String password, String locatorPassword,
                                            String locatorLogInButton){
        Utils.LOG.info("LOADING LOGIN PAGE! ");
        waitForElementVisible(locatorUserName,1);
        clearTextInField(locatorUserName);
        typeValueInField(userName,locatorUserName);
        clearTextInField(locatorPassword);
        typeValueInField(password,locatorPassword);
        clickElement(locatorLogInButton);
        assertElementNotPresent(locatorLogInButton);
    }

    //DDT
    public void checkingInsuranceCalculator(String carMake, String carModel,
                                            String cubicCapacity, String firstRegistration,
                                            String hasAccident, String driverAge,
                                            String totalPremium) throws InterruptedException {
        //Car make
        Select selectCarMake = new Select(driver.findElement(By.xpath("//select[@id='carBrand']")));
        selectCarMake.selectByVisibleText(carMake);
        //Car model
        Select selectCarModel = new Select(driver.findElement(By.xpath("//select[@id='carModel']")));
        selectCarModel.selectByVisibleText(carModel);
        //Cubic capacity
        typeValueInField(cubicCapacity,Utils.getUIMappingByKey("CalculatorPage.CubicCapacity"));
        //First registration
        typeValueInField(firstRegistration,Utils.getUIMappingByKey("CalculatorPage.FirstRegistration"));
        //Had accident
        if(hasAccident.equals("CHECK")){
            clickElement(Utils.getUIMappingByKey("CalculatorPae.CheckBox"));
        }
        //Driver Age
        typeValueInField(driverAge,Utils.getUIMappingByKey("CalculatorPage.DriverAge"));
        //Click on button calculate
        clickElement(Utils.getUIMappingByKey("CalculatorPae.CalculateButton"));
        Utils.LOG.info("Result: " + Utils.getUIMappingByKey("CalculatorPage.TotalPremium") + " expected result: " + totalPremium);
        Assert.assertEquals(Utils.getUIMappingByKey("CalculatorPage.TotalPremium"),totalPremium);

    }
}

