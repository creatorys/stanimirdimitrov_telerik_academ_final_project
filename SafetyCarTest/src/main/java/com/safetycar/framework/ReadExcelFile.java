package com.safetycar.framework;
import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
import jdk.nashorn.internal.runtime.NumberToString;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.util.NumberToTextConverter;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.DataOutput;
import java.io.File;
import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReadExcelFile {
    XSSFWorkbook wb;
    XSSFSheet sheet;

    public void readExcelFile(String excelPath){
        try{
            File src = new File(excelPath);
            FileInputStream fis = new FileInputStream(src);
            wb = new XSSFWorkbook(fis);
        }catch(Exception e)
        {
            Utils.LOG.error(e.getMessage());
        }
    }

    public String getData(int sheetNumber, int row, int column){

        sheet = wb.getSheetAt(sheetNumber);
        String data="";
        if(sheet.getRow(row).getCell(column).getCellType().equals(CellType.STRING)){
            data = sheet.getRow(row).getCell(column).getStringCellValue();
        }else {
            data=NumberToTextConverter.toText(Double.parseDouble(sheet.getRow(row).getCell(column).getRawValue()));
        }
        Utils.LOG.info("Value is: " + data);
        return data;
    }

    public int getRowCount(int sheetIndex){
        int row = wb.getSheetAt(sheetIndex).getLastRowNum();
        row = row +1;
        return row;
    }
}
