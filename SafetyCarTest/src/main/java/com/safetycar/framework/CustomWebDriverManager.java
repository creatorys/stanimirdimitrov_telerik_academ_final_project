package com.safetycar.framework;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.SessionId;

public class CustomWebDriverManager {
    public enum CustomWebDriverManagerEnum{
        INSTANCE;
        private static String browser = System.getProperty("browser");
        private static WebDriver driver = setupBrowser();

        //Use firefox via ide with Web Driver Manager
        private static WebDriver setupBrowser() {
            WebDriverManager.firefoxdriver().setup();
            WebDriver firefoxDriver = new FirefoxDriver();
            firefoxDriver.manage().window().maximize();
            return firefoxDriver;
        }

        //Choose browser from command line
        private static WebDriver setupBrowser(String userBrowser) {
            if(userBrowser.equalsIgnoreCase("chrome")){
                WebDriverManager.chromedriver().setup();
                WebDriver chromeDriver = new ChromeDriver();
                chromeDriver.manage().window().maximize();
                driver = chromeDriver;
            }
            else if(userBrowser.equalsIgnoreCase("firefox") ||
            userBrowser.equalsIgnoreCase("mozilla")  ||
            userBrowser.equalsIgnoreCase("")) {
                WebDriverManager.firefoxdriver().setup();
                WebDriver firefoxDriver = new FirefoxDriver();
                firefoxDriver.manage().window().maximize();
                driver = firefoxDriver;
            }
            else{
                System.out.println("Use only Chrome and Firefox");
            }
            return  driver;
        }

        public void quitDriver(){
            if(driver != null){
                driver.quit();
            }
        }

        public WebDriver getDriver(){

            // Condition when firefox is used via the IDE
            SessionId session = ((FirefoxDriver)driver).getSessionId();

            if(session==null)
            {
                driver = setupBrowser();
            }
            return driver;
        }
    }
}
