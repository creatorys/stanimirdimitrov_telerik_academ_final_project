<div align="center">
#Safety car
</div>

## TABLE OF CONTENTS
[I. INTRODUCTION])<br>
[II. QA])<br>
[III. DELIVERABLES])<br>
[III. USEFUL LINKS])<br>
[IV. HOW TO RUN AUTOMATION])<br>

## INTRODUCTION

__SAFETY CAR__ is aplication that allow user to send request for insurance policy)<br>
- Registation
- Send request
- Get offer
- Cencel request
- User can see all his requests
- Change profile information
 
## QA:
Stanimir Dimitrov

## DELIVERABLES
__Test plan__ - (https://drive.google.com/open?id=1P4w5xyfmP_RcNEBdZjYSu8zM7M5WZ9Ae)<br>
__Bug tracking system__ - [GitLab] (https://gitlab.com/creatorys/stanimirdimitrov_telerik_academ_final_project/issues)<br>
__Test Cases excel__ -(https://drive.google.com/open?id=1R99N89P9WIAXYeiYEmdskl1rvKzZPPgI)<br>
__Test Cases Test Link__ - (https://drive.google.com/open?id=1otBg0g_iNc8qez57I20SzvHtzALwj6fl)<br>
__Database Analysis__ -  (https://drive.google.com/open?id=1EV_FoMGuSF7vL5TCWoTMxuHZzss4DzYi)<br>
__Projecr Requirements__ -  (https://drive.google.com/open?id=1XjCU2iTyJfTxkQwpH3nsSj49C9guO_yx)<br>
__Trllo Board__ -  (https://trello.com/b/lgsf1DmY/safety-car-qa-board)<br>
__TestLink__ - (http://23.251.157.196/index.php) - user guest - password 12345)<br>
__Presentation__ - (https://drive.google.com/open?id=1o-orK_2Qv23fpjezzsC9g4xU9XA75fdC)<br>

## HOW TO RUN AUTOMATIONS

### __1. Pre-requisites for running automations__

To be able to run the project on localhost and run the test automations, the following programs need to be present on your machine:

- Windows 10 operating system
- JDK 8
- Java 1.8
- IntelliJ IDEA Ultimate Edition 2019.1.3 or later
- Gradle Command Line
- MariadDB v10.4.8 or later installed on localhost port 3306 with user "car",  p@ssword "password" and admin privileges
- Path to MariaDB bin folder should be added to PATH in system environment variables!
- Postman standalone v7.10.0 or later
- Newman v4.5.5 or later
- Newman HTML Extra Reporter  latest version
- Firefox latest version
- Selenium WebDriver
- Node.js v10.16.3 or later
- NPM v6.12.0 or later
- Apache Maven 3.6.0 or later
- Allure Command Line

### __2. Running the automations__


1. Clone the repo on your computer.
2. Run run_automation.bat.

### __3. How to run DEV project individually__

__3.1. Manually via IntelliJ IDE__

1. Open IntelliJ Ultimate Edition.
2. Open DEV Project with Use Auto-Import option checked.
3. Build Project.
4. Connect MariadDB server on localhost:3306.
5. Create new schema "safety_car".
6. Open DataBaseScript.sql in dbscript folder.
7. Select "savety_car" as schema.
8. Select all in script.sql file.
9. Execute script.
10. Start application from SafetyCarApplication class.


### __4. How to run API tests automation individually__

1. Run runPostmanCollectionS.bat on the landing page of the repo.
2. Via GitLab CI
3. Automation Run every hour with postman monitor


### __5. How to run UI tests automation individually__

1. Create the database and load test data as described in section 3.2.
2. Start Dev Application as described in section 3.2.
3. Run run_ui_automation.bat file at the root location of the repo.

If automation runs fine, an Allure HTML report should be present at the end in your default browser.

__NB:__ If you would like to run a certain test suite, you need to add one additional parameter in the run_ui_automation.bat: -DrunSuite="Name of java class without the .class extension":

e.g. mvn clean test -Dbrowser="firefox" -DrunSuite="Test01_CreateAcountNegative"

